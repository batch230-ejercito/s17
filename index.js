/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/

	function printWelcomeMessage(){
	let fullName =prompt("Enter your Fullname: ");
	let age =prompt("Enter your age: ");
	let location =prompt("Enter your location: ");
 	console.log("Hello, "+ fullName);
 	console.log("You are " + age, "old");
 	console.log("You Lived in " +location);

	
 }
 printWelcomeMessage();

	
	//first function here:




/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
 		function printFavoriteBand(){
 			console.log("1. The Beatles");
 			 console.log("2. Metallica");
 			 console.log("3. The Eagles");
 		     console.log("4. L'arc~en~ciel");
 		     console.log("5. Eraserheads");
 	

 		}

 		printFavoriteBand();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	*/
 		function favMovies(){
 			console.log("1. The GodFather");
 			console.log("Rotten Tomatoes Rating: 97%");
 			 console.log("2. GodFather, Part II");
 			 console.log("Rotten Tomatoes Rating: 96%");
 			 console.log("3. Shawshank Redmption");
 			 console.log("Rotten Tomatoes Rating: 91%");
 			 console.log("4. To Kill A Mockingbird");
 			 console.log("Rotten Tomatoes Rating: 93%");
 		     console.log("5. Psycho");
 		     console.log("Rotten Tomatoes Rating: 96%");
 	
 	

 		}

 		favMovies();
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

 // function printUsers(){
 //      printFriends = function printUsers(){
 //     alert("Hi! Please add the names of your friends.");
 // 	     let friend1 = promt("Enter your first friend's name:"); 
// 	     let friend2 = prompt("Enter your second friend's name:"); 
// 	     let friend3 = prompt("Enter your third friend's name:");
 // 	console.log(`You are friends with:, ${friend1} ${friend2} ${friend3}!`);
// 	// console.log(friend1); 
// 	// console.log(friend2); 
// 	// console.log(friends); 
// 	// console.log('printFriends)
// }
// printFriends();

// }

// // console.log(friend1);
// // console.log(friend2);
// // console.log(friends); 

 // printUsers();
    let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}
	printFriends();


// console.log(friend1);
// console.log(friend2);
